package com.bkeen.ems;

import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.text.InputType;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.ksoap2.serialization.SoapObject;

import java.util.HashMap;

/**
 * Created by chiulandice on 11/6/15.
 */
public class ElectricCarDialog extends BaseDialog implements Connect, View.OnClickListener {

    private Context mContext;

    private ImageButton btn_close_metalfram;
    private ImageView iv_charge_image;
    private TextView tv_discharge;
    private TextView tv_power_value;
    private TextView tv_power_value_unit;
    private ImageButton imb_toggle_charge;
    private ImageButton btn_charge;
    private ImageButton btn_release;
    private TextView tv_charge_power_time;
    private TextView tv_charge_power_time_value;
    private TextView tv_charge_power_time_need;
    private TextView tv_charge_power_time_need_value;
    private TextView tv_battery_info_temp_value;
    private TextView tv_battery_info_voltage_value;
    private TextView tv_battery_info_current_value;
    private TextView tv_battery_info_quantity_value;
    private TextView tv_target_power_value;
    private Button btn_check;
    private ImageButton imb_input_target_value;

    private HttpAsyncTask asyncTask_EV_80A_charging;
    private HttpAsyncTask asyncTask_ESS_charging;
    private HttpAsyncTask asyncTask_EV_80A_Power;
    private int currentValue = 0;

    private Handler mHandler = new Handler();

    Runnable getValue = new Runnable() {
        @Override
        public void run() {

            //加入參數 HTTP_Meter = Meter , ESS = ESS , HTTP_Power = Power
            //最終Get路徑 :HTTP_EMS_Root_URL / HTTP_Meter / ESS / Power
            asyncTask_EV_80A_charging.connect();
            asyncTask_EV_80A_Power.connect();
//            asyncTask_ESS_charging.connect();
            mHandler.postDelayed(this, 2000);
        }
    };

    Runnable dismissprogress = new Runnable() {
        @Override
        public void run() {
            if(progressdialog != null){
                progressdialog.dismiss();
                Toast.makeText(mContext, "指令已傳送", Toast.LENGTH_LONG).show();
            }
        }
    };

    // 0: not define state.
    // 1: battery charge.
    // 2: battery release.
    private int batteryState = 0;

    private static String TAG = "ElectricCarDialog";

    public ElectricCarDialog(Context context) {
        super(context);
        mContext = context;
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.dialog_electric_car_layout);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        btn_close_metalfram = (ImageButton) this.findViewById(R.id.btn_close_metalfram);
        btn_close_metalfram.setOnClickListener(this);

        imb_toggle_charge = (ImageButton) this.findViewById(R.id.imb_toggle_charge);
        imb_toggle_charge.setOnClickListener(this);

        btn_charge = (ImageButton) this.findViewById(R.id.btn_charge);
        btn_charge.setOnClickListener(this);

        btn_release = (ImageButton) this.findViewById(R.id.btn_release);
        btn_release.setOnClickListener(this);

        btn_check = (Button) this.findViewById(R.id.btn_check);
        btn_check.setOnClickListener(this);

        imb_input_target_value = (ImageButton) this.findViewById(R.id.imb_input_target_value);
        imb_input_target_value.setOnClickListener(this);

        iv_charge_image = (ImageView) this.findViewById(R.id.iv_charge_image);
        tv_discharge = (TextView) this.findViewById(R.id.tv_discharge);
        tv_power_value = (TextView) this.findViewById(R.id.tv_power_value);
        tv_power_value_unit = (TextView) this.findViewById(R.id.tv_power_value_unit);
        tv_charge_power_time = (TextView) this.findViewById(R.id.tv_charge_power_time);
        tv_charge_power_time_value = (TextView) this.findViewById(R.id.tv_charge_power_time_value);
        tv_charge_power_time_need = (TextView) this.findViewById(R.id.tv_charge_power_time_need);
        tv_charge_power_time_need_value = (TextView) this.findViewById(R.id.tv_charge_power_time_need_value);
//        tv_battery_info_temp_value = (TextView) this.findViewById(R.id.tv_battery_info_temp_value);
        tv_battery_info_voltage_value = (TextView) this.findViewById(R.id.tv_battery_info_voltage_value);
        tv_battery_info_current_value = (TextView) this.findViewById(R.id.tv_battery_info_current_value);
        tv_battery_info_quantity_value = (TextView) this.findViewById(R.id.tv_battery_info_quantity_value);
        tv_target_power_value = (TextView) this.findViewById(R.id.tv_target_power_value);

//        setDefault();

//        batteryState = 2;
//        setRelease();

//        if (Config.ELECTRIC_CAR_STATE) {
//            // charging
//            setCharge();
//            btn_charge.setVisibility(View.VISIBLE);
//            btn_release.setVisibility(View.GONE);
//            batteryState = 1;
//        }else {
//            setRelease();
//            btn_charge.setVisibility(View.GONE);
//            btn_release.setVisibility(View.VISIBLE);
//            batteryState = 2;
//        }
//
//        tv_power_value.setText(Config.ELECTRIC_CAR_VALUE);
//        String temp = Config.ELECTRIC_CAR_VALUE;
//        if (temp.length() != 0) {
//            String current = Config.doubleDecimalFormat(String.valueOf(Double.valueOf(temp) / 220));
//            tv_battery_info_current_value.setText(current);
//        }

        tv_power_value.setText(Config.ELECTRIC_CAR_VALUE);
        String temp = Config.ELECTRIC_CAR_VALUE;
        if (temp.length() != 0) {
            String current = Config.doubleDecimalFormat(String.valueOf(Double.valueOf(temp) / 220));
            tv_battery_info_current_value.setText(current);
            if (Double.parseDouble(temp) >0) {
                batteryState = 1;
                setCharge();
                btn_charge.setVisibility(View.VISIBLE);
                btn_release.setVisibility(View.GONE);
            }else if (Double.parseDouble(temp) <0) {
                batteryState = 2;
                setRelease();
                btn_charge.setVisibility(View.GONE);
                btn_release.setVisibility(View.VISIBLE);
            }
        }
    }

    @Override
    protected void onStart() {
        super.onStart();

        asyncTask_EV_80A_Power = new HttpAsyncTask(Config.ACTION_getEV_80A_Power,this,Config.HTTP_EMS_Root_URL,Config.HTTP_Meter,Config.EV_80A,Config.HTTP_Power);
        asyncTask_EV_80A_charging = new HttpAsyncTask(Config.ACTION_getEV_80A_charging_info, this,Config.HTTP_EMS_Root_URL,Config.HTTP_EnergyStorage,Config.EV_80A);
//        asyncTask_ESS_charging = new HttpAsyncTask(Config.ACTION_getESS_charging_info,this,Config.HTTP_EMS_Root_URL,Config.HTTP_EnergyStorage,Config.ESS);

        mHandler.postDelayed(getValue, 2000);

        Log.i(TAG, "初次進入微電網");
        // Station_No:8992
        // Set_No:00000002
        // Socket_no:00000002
        // 建立Soap物件
        SoapObject sensor = new SoapObject(Config.NAMESPACE_Mpc4Ems,Config.METHOD_NAME_c800);
        sensor.addProperty("station_no", "8992");
        sensor.addProperty("set_no", 00000002);
        // In to ems mode.
        sensor.addProperty("set_mode", "01");
        //綁定onResult
        SoapAsyncTask asyncTask = new SoapAsyncTask("First into EMS",this,sensor);
        //開始異步連線
        asyncTask.execute(Config.URL_Mpc4Ems,Config.SOAP_ACTION_c800);


        // Test SOC
//        SoapObject sensor1 = new SoapObject(Config.NAMESPACE_Mpc4Ems,Config.METHOD_NAME_c805);
//        sensor1.addProperty("station_no", "8992");
//        sensor1.addProperty("set_no", 00000002);
//        sensor1.addProperty("Socket_No", 00000002);
//        //綁定onResult
//        SoapAsyncTask asyncTask1 = new SoapAsyncTask("GET SOC",this,sensor1);
//        //開始異步連線
//        asyncTask1.execute(Config.URL_Mpc4Ems,Config.SOAP_ACTION_c805);
    }

    @Override
    public void dismiss() {
        super.dismiss();
        mHandler.removeCallbacks(getValue);
    }

    @Override
    public void onClick(View v) {
        int _id = v.getId();
        switch (_id) {
            case R.id.btn_close_metalfram: {
                Log.i(TAG, "離開微電網");
                // Close this popup dialog window.
                SoapObject sensor = new SoapObject(Config.NAMESPACE_Mpc4Ems, Config.METHOD_NAME_c800);
                sensor.addProperty("station_no", "8992");
                sensor.addProperty("set_no", 00000002);
                sensor.addProperty("set_mode", "02");
                //綁定onResult
                SoapAsyncTask asyncTask = new SoapAsyncTask("leave EMS", this, sensor);
                //開始異步連線
                asyncTask.execute(Config.URL_Mpc4Ems, Config.SOAP_ACTION_c800);
                cancel();
                break;
            }

            case R.id.btn_charge:
                // Click charge button.
                btn_charge.setVisibility(View.VISIBLE);
                btn_release.setVisibility(View.GONE);
                batteryState = 1;
                break;

            case R.id.btn_release:
                // Click release button.
                btn_charge.setVisibility(View.GONE);
                btn_release.setVisibility(View.VISIBLE);
                batteryState = 2;
                break;

            case R.id.imb_toggle_charge:
                // Click to change charge or release state.
                switch (batteryState) {
                    case 0:
                        break;
                    case 1:
                        // Origin is charge, change to release.
                        btn_charge.setVisibility(View.GONE);
                        btn_release.setVisibility(View.VISIBLE);
                        batteryState = 2;
                        break;
                    case 2:
                        // Origin is release, change to charge.
                        btn_charge.setVisibility(View.VISIBLE);
                        btn_release.setVisibility(View.GONE);
                        batteryState = 1;
                        break;
                }
                break;

            case R.id.btn_check:
                // Close this popup dialog window.
                // 轉圈圈
//                Toast.makeText(this.getContext(), "指令已傳送", 2000).show();
                switch (batteryState) {
                    case 1: {
                        Log.i(TAG, "Soap 設定充電");
                        // Station_No:8992
                        // Set_No:00000002
                        // Socket_no:00000002
                        // 建立Soap物件
//                        Log.i(TAG, "設定強制停放");
//                        SoapObject sensor = new SoapObject(Config.NAMESPACE_Mpc4Ems, Config.METHOD_NAME_c803);
//                        sensor.addProperty("station_no", "8992");
//                        sensor.addProperty("set_no", 00000002);
//                        // In to ems mode.
//                        sensor.addProperty("socket_no", 00000002);
//                        //綁定onResult
//                        SoapAsyncTask asyncTask = new SoapAsyncTask("stop action", this, sensor);
//                        //開始異步連線
//                        asyncTask.execute(Config.URL_Mpc4Ems, Config.SOAP_ACTION_c803);


                        Log.i(TAG, "設定開始充電");
                        SoapObject sensor1 = new SoapObject(Config.NAMESPACE_Mpc4Ems, Config.METHOD_NAME_c801);
                        sensor1.addProperty("station_no", "8992");
                        sensor1.addProperty("set_no", 00000002);
                        sensor1.addProperty("Socket_No", 00000002);
                        sensor1.addProperty("Charge_Value", 80);
                        //綁定onResult
                        SoapAsyncTask asyncTask1 = new SoapAsyncTask("start charge action", this, sensor1);
                        //開始異步連線
                        asyncTask1.execute(Config.URL_Mpc4Ems, Config.SOAP_ACTION_c801);
                        break;
                    }
                    case 2: {
                        Log.i(TAG, "Soap 設定放電");
                        // Station_No:8992
                        // Set_No:00000002
                        // Socket_no:00000002
                        // 建立Soap物件
//                        Log.i(TAG, "設定強制停充");
//                        SoapObject sensor = new SoapObject(Config.NAMESPACE_Mpc4Ems, Config.METHOD_NAME_c830);
//                        sensor.addProperty("station_no", "8992");
//                        sensor.addProperty("set_no", 00000002);
//                        // In to ems mode.
//                        sensor.addProperty("socket_no", 00000002);
//                        //綁定onResult
//                        SoapAsyncTask asyncTask = new SoapAsyncTask("stop action", this, sensor);
//                        //開始異步連線
//                        asyncTask.execute(Config.URL_Mpc4Ems, Config.SOAP_ACTION_c830);

                        Log.i(TAG, "設定開始放電");
                        if (currentValue == 0) {
                            Toast.makeText(mContext, "電流範圍不合", Toast.LENGTH_SHORT).show();
                            return;
                        }
                        SoapObject sensor1 = new SoapObject(Config.NAMESPACE_Mpc4Ems, Config.METHOD_NAME_c802);
                        sensor1.addProperty("station_no", "8992");
                        sensor1.addProperty("set_no", 00000002);
                        sensor1.addProperty("Socket_No", 00000002);
                        sensor1.addProperty("Charge_Value", currentValue);
                        //綁定onResult
                        SoapAsyncTask asyncTask1 = new SoapAsyncTask("start release action", this, sensor1);
                        //開始異步連線
                        asyncTask1.execute(Config.URL_Mpc4Ems, Config.SOAP_ACTION_c802);
                        break;
                    }
                }
//                showDialog();
                progressdialog = new MaterialDialog.Builder(mContext)
                        .content("wait...")
                        .progress(true, 0)
                        .cancelable(false)
                        .show();
                mHandler.postDelayed(dismissprogress, 2000);
                break;

            case R.id.imb_input_target_value:
                new MaterialDialog.Builder(mContext)
                        .title("設定目標電流")
                        .content("")
                        .inputType(InputType.TYPE_CLASS_NUMBER)
                        .input("請輸入數字", "", new MaterialDialog.InputCallback() {
                            @Override
                            public void onInput(MaterialDialog dialog, CharSequence input) {
                                // currentValue
                                if (input.toString().length() == 0 || input.toString() == null) {
                                    Toast.makeText(mContext, "輸入不可為空", Toast.LENGTH_SHORT).show();
                                }else {
                                    int temp = Integer.valueOf(input.toString());
                                    if (temp > 80 || temp <= 0) {
                                        Toast.makeText(mContext, "輸入範圍不合", Toast.LENGTH_SHORT).show();
                                    } else {
                                        currentValue = temp;
                                        tv_target_power_value.setText(String.valueOf(temp));
                                    }
                                }
                            }
                        }).show();
                break;
        }
    }

    // Set view on default state.
    void setDefault() {

    }

    // Set view on charge state.
    void setCharge() {
//        btn_charge.setVisibility(View.VISIBLE);
//        btn_release.setVisibility(View.GONE);

        iv_charge_image.setImageResource(R.drawable.electricity_charging);
        tv_discharge.setText("充電中");
        tv_discharge.setTextColor(0xffff6f00);
        tv_power_value.setTextColor(0xffff6f00);
        tv_power_value_unit.setTextColor(0xffff6f00);
        tv_charge_power_time.setTextColor(0xffffffff);
        tv_charge_power_time_value.setTextColor(0xffffffff);
        tv_charge_power_time_need.setTextColor(0xffffffff);
        tv_charge_power_time_need_value.setTextColor(0xffffffff);
//        tv_charge_power_time_value.setText("34min");
//        tv_charge_power_time_need_value.setText("10min");
    }

    // Set view on release state.
    void setRelease() {
//        btn_charge.setVisibility(View.GONE);
//        btn_release.setVisibility(View.VISIBLE);

        iv_charge_image.setImageResource(R.drawable.electricity_release);
        tv_discharge.setText("放電中");
        tv_discharge.setTextColor(0xff00ff2a);
        tv_power_value.setTextColor(0xff00ff2a);
        tv_power_value_unit.setTextColor(0xff00ff2a);
        tv_charge_power_time.setTextColor(0xff978c8c);
        tv_charge_power_time_value.setTextColor(0xff978c8c);
        tv_charge_power_time_need.setTextColor(0xff978c8c);
        tv_charge_power_time_need_value.setTextColor(0xff978c8c);
//        tv_charge_power_time_value.setText("---");
//        tv_charge_power_time_need_value.setText("---");
    }

    @Override
    public void onResult(String SOAP_ACTION, SoapObject result) {
        Log.i(TAG, "onResult:" + SOAP_ACTION + " , object result" + result);
        if(result == null){
            Log.d(TAG, "result is null.");
            return;
        }

        if ("stop action".equals(SOAP_ACTION)) {
            Log.i(TAG, "get soap response charge");
        }else if ("start release action".equals(SOAP_ACTION)) {
            Log.i(TAG, "get release soap response charge");
            Log.i(TAG, "msg:"+result.toString());
        }else if ("start charge action".equals(SOAP_ACTION)) {
            Log.i(TAG, "get charge soap response charge");
            Log.i(TAG, "msg:"+result.toString());
        }else if ("First into EMS".equals(SOAP_ACTION)) {
            Log.i(TAG, "Into EMS");
        }else if ("leave EMS".equals(SOAP_ACTION)) {
            Log.i(TAG, "Leave EMS");
        }else if ("GET SOC".equals(SOAP_ACTION)) {
            Log.i(TAG, "GET SOC");
        }
    }

    //Http回傳
    @Override
    public void onHttpResult(String HTTP_ACTION, String result) {

        if(HTTP_ACTION.equals(Config.ACTION_getEV_80A_charging_info)) { //取得電動車充放電資訊
            Log.i(TAG, HTTP_ACTION + " : " + result);

            try{
//                onSuccess: ["Charge":false, "Power":0.0, "SOC":73.0]
//                onSuccess: ["Charge":false, "Power":0.0, "SOC":999.0]

                HashMap<String,String> retList = new Gson().fromJson(result.replace("[","{").replace("]","}"),
                        new TypeToken<HashMap<String,String>>() {
                        }.getType());
                String charge = retList.get("Charge");
                Double power = Double.parseDouble(retList.get("Power"));
                Double soc = Double.parseDouble(retList.get("SOC"));
                Log.d(TAG, "Charge:" + charge);
                Log.d(TAG, "Power:" + power);
                Log.d(TAG, "SOC:" + soc);
                //true：充電中   false：放電中
//                if ("false".equals(charge)) {
                    // Release
//                    setRelease();
//                    batteryState = 2;
//                    if (power == 0) {
//                        tv_power_value.setText("---");
//                    }else {
//                        tv_power_value.setText(String.valueOf(power));
//                    }
//                }else {
                    // Charge
//                    setCharge();
//                    batteryState = 1;
//                    if (power == 0) {
//                        tv_power_value.setText("---");
//                    }else {
//                        tv_power_value.setText(String.valueOf(power));
//                    }
//                }
            }catch (Exception e){

            }

        }else if (HTTP_ACTION.equals(Config.ACTION_getEV_80A_Power)) {
            Log.d(TAG, HTTP_ACTION + " : " + result);
            String temp = Config.doubleDecimalFormat(result);
            if (temp.length() >0) {
                tv_power_value.setText(temp);
                String current = Config.doubleDecimalFormat(String.valueOf(Double.valueOf(temp) / 220));
                tv_battery_info_current_value.setText(current);
//            if (batteryState == 2) {
//                // On release
//
//                if (Double.valueOf(temp) > 0) {
//                    tv_power_value.setText("-" + temp);
//                }
//            }
                if (Double.parseDouble(temp) > 0) {
                    batteryState = 1;
                    setCharge();
                } else if (Double.parseDouble(temp) < 0) {
                    batteryState = 2;
                    setRelease();
                }
            }
        }
    }

    HttpAsyncTask asyncTask;
    MaterialDialog progressdialog;
    private void showDialog(){

        new MaterialDialog.Builder(mContext)
                .content("確認發送指令?")
                .positiveText("確認")
                .negativeText("取消")
                .onPositive(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                        Log.d(TAG, "onClick");

                        progressdialog = new MaterialDialog.Builder(mContext)
                                .content("wait...")
                                .progress(true, 0)
                                .cancelable(false)
                                .show();
                        mHandler.postDelayed(dismissprogress, 2000);
                    }
                })
                .autoDismiss(true)
                .cancelable(false)
                .show();
    }
}
