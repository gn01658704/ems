package com.bkeen.ems;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.view.Window;

/**
 * Created by Pototype on 2015/11/4.
 */
public class BaseDialog extends Dialog {

    public BaseDialog(Context context) {
        super(context, R.style.dialog);
        setCanceledOnTouchOutside(false);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
    }
}
