package com.bkeen.ems;

import android.app.Activity;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.util.Log;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.ImageButton;
import android.widget.RadioGroup;
import android.widget.ToggleButton;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.lidroid.xutils.ViewUtils;

import org.ksoap2.serialization.SoapObject;

/**
 * Created by Pototype on 2015/11/4.
 */
public class DigitalIODialog extends BaseDialog implements CompoundButton.OnCheckedChangeListener ,Connect{
    private String TAG="DigitalIODialog";
    ImageButton btn_close_metalfram;
    Activity mContext;
    ToggleButton toggleButton_DIO01;
    ToggleButton toggleButton_DIO02;
    ToggleButton toggleButton_DIO04;
    ToggleButton toggleButton_DIO05;
    ToggleButton toggleButton_DIO10;
    private Handler mHandler = new Handler();
    public DigitalIODialog(Activity context) {
        super(context);
        mContext = context;
        setContentView(R.layout.dialog_metalframe_controll_layout);
        setCanceledOnTouchOutside(false);
        Log.d(TAG, "DigitalIODialog");
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d(TAG, "onCreate");

        toggleButton_DIO01 = (ToggleButton) findViewById(R.id.toggleButton_DIO01);
        toggleButton_DIO02 = (ToggleButton) findViewById(R.id.toggleButton_DIO02);
        toggleButton_DIO04 = (ToggleButton) findViewById(R.id.toggleButton_DIO04);
        toggleButton_DIO05 = (ToggleButton) findViewById(R.id.toggleButton_DIO05);
        toggleButton_DIO10 = (ToggleButton) findViewById(R.id.toggleButton_DIO10);

        toggleButton_DIO01.setChecked(Config.DIO01_state);
        toggleButton_DIO02.setChecked(Config.DIO02_state);
        toggleButton_DIO04.setChecked(Config.DIO04_state);
        toggleButton_DIO05.setChecked(Config.DIO05_state);
        toggleButton_DIO10.setChecked(Config.DIO10_state);

        toggleButton_DIO01.setOnCheckedChangeListener(this);
        toggleButton_DIO02.setOnCheckedChangeListener(this);
        toggleButton_DIO04.setOnCheckedChangeListener(this);
        toggleButton_DIO05.setOnCheckedChangeListener(this);
        toggleButton_DIO10.setOnCheckedChangeListener(this);

        btn_close_metalfram = (ImageButton) findViewById(R.id.btn_close_metalfram);
        btn_close_metalfram.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dismiss();
            }
        });

    }

    @Override
    protected void onStart() {
        super.onStart();
        Log.d(TAG, "onStart");
    }

    @Override
    protected void onStop() {
        super.onStop();
        Log.d(TAG, "onStop");
    }

    @Override
    public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {
        showDialog(compoundButton.getId(),isChecked);
    }

    HttpAsyncTask asyncTask;
    MaterialDialog progressdialog;
    private void showDialog(final int toggleButton_id, final boolean isChecked){


        switch (toggleButton_id) {

            case R.id.toggleButton_DIO01:
                asyncTask = new HttpAsyncTask(Config.ACTION_setDIO01, DigitalIODialog.this,
                        Config.HTTP_EMS_Root_URL, Config.HTTP_DigitalIO, "1", isChecked ? "1" : "0");
                break;

            case R.id.toggleButton_DIO02:
                asyncTask = new HttpAsyncTask(Config.ACTION_setDIO02, DigitalIODialog.this,
                        Config.HTTP_EMS_Root_URL, Config.HTTP_DigitalIO, "2", isChecked ? "1" : "0");
                break;

            case R.id.toggleButton_DIO04:
                asyncTask = new HttpAsyncTask(Config.ACTION_setDIO04, DigitalIODialog.this,
                        Config.HTTP_EMS_Root_URL, Config.HTTP_DigitalIO, "4", isChecked ? "1" : "0");
                break;

            case R.id.toggleButton_DIO05:
                asyncTask = new HttpAsyncTask(Config.ACTION_setDIO05, DigitalIODialog.this,
                        Config.HTTP_EMS_Root_URL, Config.HTTP_DigitalIO, "5", isChecked ? "1" : "0");
                break;

            case R.id.toggleButton_DIO10:
                asyncTask = new HttpAsyncTask(Config.ACTION_setDIO10, DigitalIODialog.this,
                        Config.HTTP_EMS_Root_URL, Config.HTTP_DigitalIO, "10", isChecked ? "1" : "0");
                break;
        }

        asyncTask.connect();
        progressdialog = new MaterialDialog.Builder(mContext)
                .content("wait...")
                .progress(true, 0)
                .cancelable(false)
                .show();
        mHandler.postDelayed(dismissprogress, 2000);

    }


    Runnable dismissprogress = new Runnable() {
        @Override
        public void run() {
            if(progressdialog != null){
                progressdialog.dismiss();
            }
        }
    };

    @Override
    public void onResult(String SOAP_ACTION, SoapObject result) {

    }

    @Override
    public void onHttpResult(String HTTP_ACTION, String result) {

        if(HTTP_ACTION.equals(Config.ACTION_setDIO01)){
            Log.d(TAG, HTTP_ACTION + " : " + result);
        }else if(HTTP_ACTION.equals(Config.ACTION_setDIO02)){
            Log.d(TAG, HTTP_ACTION + " : " + result);
        }else if(HTTP_ACTION.equals(Config.ACTION_setDIO04)){
            Log.d(TAG, HTTP_ACTION + " : " + result);
        }else if(HTTP_ACTION.equals(Config.ACTION_setDIO05)){
            Log.d(TAG, HTTP_ACTION + " : " + result);
        }else if(HTTP_ACTION.equals(Config.ACTION_setDIO10)){
            Log.d(TAG, HTTP_ACTION + " : " + result);
        }
    }
}
