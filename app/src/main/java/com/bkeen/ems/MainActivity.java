package com.bkeen.ems;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.widget.CompoundButton;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.ToggleButton;

import com.lidroid.xutils.ViewUtils;
import com.lidroid.xutils.view.annotation.ViewInject;

import uk.co.chrisjenx.calligraphy.CalligraphyConfig;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class MainActivity extends FragmentActivity implements  CompoundButton.OnCheckedChangeListener{
    private String TAG = "MainActivity";

    @ViewInject(R.id.btn_home)
    private RadioButton btn_home;

    @ViewInject(R.id.btn_entertainment)
    private RadioButton btn_entertainment;

    @ViewInject(R.id.btn_power)
    private RadioButton btn_power;

    @ViewInject(R.id.btn_security)
    private RadioButton btn_security;

    @ViewInject(R.id.btn_service)
    private RadioButton btn_service;

    @ViewInject(R.id.btn_setting)
    private RadioButton btn_setting;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d(TAG, "onCreate");
        CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
                .setDefaultFontPath("fonts/UGCSH00B.ttf")
                .setFontAttrId(R.attr.fontPath)
                .build());
        setContentView(R.layout.activity_main);
        ViewUtils.inject(this);
    }


    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));

    }


    @Override
    protected void onResume() {
        super.onResume();
        btn_home.setOnCheckedChangeListener(this);
        btn_entertainment.setOnCheckedChangeListener(this);
        btn_power.setOnCheckedChangeListener(this);
        btn_security.setOnCheckedChangeListener(this);
        btn_service.setOnCheckedChangeListener(this);
        btn_setting.setOnCheckedChangeListener(this);


        btn_power.setChecked(true);
    }

    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        switch (buttonView.getId()){
            case R.id.btn_home:
                if(isChecked) {
                    btn_entertainment.setChecked(false);
                    btn_power.setChecked(false);
                    btn_security.setChecked(false);
                    btn_service.setChecked(false);
                    btn_setting.setChecked(false);
                    OtherActivityFragment fragment = new OtherActivityFragment();
                    fragmentTransaction.replace(R.id.layout_fragment, fragment, "btn_home");
                    fragmentTransaction.commit();
                }else{
                    Log.d(TAG, "remove");
                    OtherActivityFragment fragment = (OtherActivityFragment)fragmentManager.findFragmentByTag("btn_home");
                    fragmentTransaction.remove(fragment);
                    fragmentTransaction.commit();
                }
                break;

            case R.id.btn_entertainment:
                if(isChecked) {
                    btn_home.setChecked(false);
                    btn_power.setChecked(false);
                    btn_security.setChecked(false);
                    btn_service.setChecked(false);
                    btn_setting.setChecked(false);
                    OtherActivityFragment fragment = new OtherActivityFragment();
                    fragmentTransaction.replace(R.id.layout_fragment, fragment, "btn_entertainment");
                    fragmentTransaction.commit();
                }else{
                    Log.d(TAG, "remove");
                    OtherActivityFragment fragment = (OtherActivityFragment)fragmentManager.findFragmentByTag("btn_entertainment");
                    fragmentTransaction.remove(fragment);
                    fragmentTransaction.commit();
                }
                break;

            case R.id.btn_power:
                Log.d(TAG, "isChecked:" + isChecked);

                if(isChecked){
                    btn_home.setChecked(false);
                    btn_entertainment.setChecked(false);
                    btn_security.setChecked(false);
                    btn_service.setChecked(false);
                    btn_setting.setChecked(false);
                    EntertainmentActivityFragment fragment = new EntertainmentActivityFragment();
                    fragmentTransaction.replace(R.id.layout_fragment,fragment,"btn_power");
                    fragmentTransaction.commit();
                }else{
                    Log.d(TAG, "remove");
                    EntertainmentActivityFragment fragment = (EntertainmentActivityFragment)fragmentManager.findFragmentByTag("btn_power");
                    fragmentTransaction.remove(fragment);
                    fragmentTransaction.commit();
                }

                break;

            case R.id.btn_security:
                if(isChecked) {
                    btn_home.setChecked(false);
                    btn_entertainment.setChecked(false);
                    btn_power.setChecked(false);
                    btn_service.setChecked(false);
                    btn_setting.setChecked(false);
                    OtherActivityFragment fragment = new OtherActivityFragment();
                    fragmentTransaction.replace(R.id.layout_fragment, fragment, "btn_security");
                    fragmentTransaction.commit();
                }else{
                    Log.d(TAG, "remove");
                    OtherActivityFragment fragment = (OtherActivityFragment)fragmentManager.findFragmentByTag("btn_security");
                    fragmentTransaction.remove(fragment);
                    fragmentTransaction.commit();
                }
                break;

            case R.id.btn_service:

                if(isChecked){
                    btn_home.setChecked(false);
                    btn_entertainment.setChecked(false);
                    btn_power.setChecked(false);
                    btn_security.setChecked(false);
                    btn_setting.setChecked(false);
                    OtherActivityFragment fragment = new OtherActivityFragment();
                    fragmentTransaction.replace(R.id.layout_fragment, fragment, "btn_service");
                    fragmentTransaction.commit();
                }else{
                    Log.d(TAG, "remove");
                    OtherActivityFragment fragment = (OtherActivityFragment)fragmentManager.findFragmentByTag("btn_service");
                    fragmentTransaction.remove(fragment);
                    fragmentTransaction.commit();
                }
                break;

            case R.id.btn_setting:
                if(isChecked){
                    btn_home.setChecked(false);
                    btn_entertainment.setChecked(false);
                    btn_power.setChecked(false);
                    btn_security.setChecked(false);
                    btn_service.setChecked(false);
                    OtherActivityFragment fragment = new OtherActivityFragment();
                    fragmentTransaction.replace(R.id.layout_fragment, fragment, "btn_setting");
                    fragmentTransaction.commit();
                }else{
                    Log.d(TAG, "remove");
                    OtherActivityFragment fragment = (OtherActivityFragment)fragmentManager.findFragmentByTag("btn_setting");
                    fragmentTransaction.remove(fragment);
                    fragmentTransaction.commit();
                }
                break;
        }
    }
}
