package com.bkeen.ems;

import android.os.AsyncTask;
import android.util.Log;

import com.lidroid.xutils.HttpUtils;
import com.lidroid.xutils.exception.HttpException;
import com.lidroid.xutils.http.ResponseInfo;
import com.lidroid.xutils.http.callback.RequestCallBack;
import com.lidroid.xutils.http.client.HttpRequest;

import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;

/**
 * Created by Pototype on 2015/11/1.
 */
public class HttpAsyncTask {
    private String TAG = "HttpAsyncTask";
    private Connect mConnect;
    private String Action = "";
    private String URL = "";
    private HttpUtils http;

    public HttpAsyncTask(String action ,Connect connect,String Url,String...params) {
        this.Action = action;
        this.mConnect = connect;
        this.URL = Url;
        http = new HttpUtils();
        http.configCurrentHttpCacheExpiry(500);
        for (String s :params){
            URL+= s +"/";
        }
    }


    public void connect(){

        http.send(HttpRequest.HttpMethod.GET,URL,
                new RequestCallBack<String>(){
                    @Override
                    public void onLoading(long total, long current, boolean isUploading) {
                    }

                    @Override
                    public void onSuccess(ResponseInfo<String> responseInfo) {
                        Log.d(TAG, "onSuccess: " + responseInfo.result);
                        mConnect.onHttpResult(Action, responseInfo.result);
                    }

                    @Override
                    public void onStart() {
                        Log.d(TAG, "connect: " + URL);
                    }

                    @Override
                    public void onFailure(HttpException error, String msg) {

                        Log.d(TAG, "onFailure: " + msg);
                    }
                });
    }
}
