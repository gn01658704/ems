package com.bkeen.ems;

import org.ksoap2.serialization.SoapObject;

/**
 * Created by Pototype on 2015/11/1.
 */
public interface Connect {
    void onResult(String SOAP_ACTION, SoapObject result);
    void onHttpResult(String HTTP_ACTION, String result);
}
