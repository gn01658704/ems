package com.bkeen.ems;

import java.text.DecimalFormat;

/**
 * Created by Pototype on 2015/11/1.
 */
public class Config {

    //Http Get

    //public static String HTTP_EMS_Root_URL = "http://192.168.199.49:5599/YulonEnergyWebServices/services/REST/EMS/";
    public static String HTTP_EMS_Root_URL = "http://192.168.1.74:5599/YulonEnergyWebServices/services/REST/EMS/";
    public static String HTTP_Sensor = "Sensor";
    public static String HTTP_Meter = "Meter";
    public static String HTTP_Power = "Power";
    public static String HTTP_EnergyStorage = "EnergyStorage";
    public static String HTTP_DigitalIO= "DigitalIO";
    public static String MainPower = "MainPower"; //市電

    public static String Solar= "Solar";    //太陽能
    public static String ESS= "ESS";  //儲能系統
    public static String Lighting= "Lighting"; //照明設備
    public static String EV_80A= "EV-80A"; //電動車



    //Action
    public static String ACTION_getSensor = "ACTION_getSensor";
    public static String ACTION_getLighting_Power = "ACTION_getLighting_Power";
    public static String ACTION_getSolar_Power = "ACTION_getSolar_Power";
    public static String ACTION_getESS_Power = "ACTION_getESS_Power";
    public static String ACTION_getEV_80A_Power = "ACTION_getEV_80A_Power";
    public static String ACTION_getMainPower_Power = "ACTION_getMainPower_Power";
    public static String ACTION_getEV_80A_charging = "ACTION_getEV_80A_charging";
    public static String ACTION_getESS_charging = "ACTION_getESS_charging";
    public static String ACTION_getDIO01_info = "ACTION_getDIO01_info";
    public static String ACTION_getDIO02_info = "ACTION_getDIO02_info";
    public static String ACTION_getDIO04_info = "ACTION_getDIO04_info";
    public static String ACTION_getDIO05_info = "ACTION_getDIO05_info";
    public static String ACTION_getDIO10_info = "ACTION_getDIO10_info";
    public static String ACTION_setDIO01 = "ACTION_setDIO01";
    public static String ACTION_setDIO02 = "ACTION_setDIO02";
    public static String ACTION_setDIO04 = "ACTION_setDIO04";
    public static String ACTION_setDIO05 = "ACTION_setDIO05";
    public static String ACTION_setDIO10 = "ACTION_setDIO10";




    // 201511 landice
    // Add 2action for get energy storage info.
    public static String ACTION_getEV_80A_charging_info = "ACTION_getEV_80A_charging_info";
    public static String ACTION_getESS_charging_info = "ACTION_getESS_charging_info";



    //儲存狀態用

    public static boolean DIO01_state = false;  //展示燈
    public static boolean DIO02_state = false;  //走道燈
    public static boolean DIO03_state = false;  //風扇(無法控制)
    public static boolean DIO04_state = false;  //嵌燈
    public static boolean DIO05_state = false;  //入口燈
    public static boolean DIO10_state = false;  //冷氣

    // For save energy storage state and value
    public static boolean ENERGY_STORAGE_STATE = false;
    public static String ENERGY_STORAGE_VALUE = "";
    public static String ENERGY_STORAGE_SOC = "";


    public static boolean ELECTRIC_CAR_STATE = false;
    public static String ELECTRIC_CAR_VALUE = "";


    //SOAP
    public static String NAMESPACE_Meter = "http://meter.ems.webservices.energy.yulon.com/";
    public static String URL_Meter = "http://192.168.199.49:5599/YulonEnergyWebServices/services/ElectricityMeterPort";

    public static String NAMESPACE_DigitalIO ="http://DigitalIO.ems.webservices.energy.yulon.com/";
    public static String URL_DigitalIO = "http://192.168.199.49:5599/YulonEnergyWebServices/services/DigitalIOPort";

    public static String NAMESPACE_EnergyStorage = "http://EnergyStorage.ems.webservices.energy.yulon.com/";
    public static String URL_EnergyStorage = "http://192.168.199.49:5599/YulonEnergyWebServices/services/EnergyStoragePort";

    public static String NAMESPACE_Mpc4Ems = "http://service.server.ws.mpc.yesev.yulon.com/";
    public static String URL_Mpc4Ems = "http://192.168.199.36:8090/Mpc4Ems";
//  public static String U http://192.168.1.34:8090/Mpc4Ems




    public static String NAMESPACE_Sensor = "http://sensor.ems.webservices.energy.yulon.com/";
    public static String URL_Sensor = "http://192.168.199.49:5599/YulonEnergyWebServices/services/SensorTE701RPort";
    public static String SOAP_ACTION_Sensor = "urn:GetSensorInfo";
    public static String METHOD_NAME_Sensor = "getSensorInfo";


    public static String SOAP_ACTION_GetMeterInfo = "urn:GetMeterInfo";
    public static String METHOD_NAME_GetMeterInfo= "getMeterInfo";

    public static String ACTION_getMeterInfo_Voltage = "getMeterInfo_Voltage";
    public static String SOAP_ACTION_GetMeterInfo_Voltage = "urn:GetMeterInfo_Voltage";
    public static String METHOD_NAME_getMeterInfo_Voltage= "getMeterInfo_Voltage";


    public static String SOAP_ACTION_GetMeterInfo_Power = "urn:getMeterInfo_Power";
    public static String METHOD_NAME_getMeterInfo_Power= "getMeterInfo_Power";


    public static String SOAP_ACTION_getDigitalIOInfo = "urn:GetDigitalIOInfo";
    public static String METHOD_NAME_getDigitalIOInfo= "getDigitalIOInfo";




    public static String SOAP_ACTION_getEnergyStorageInfo_SOC = "urn:GetEnergyStorageInfo_SOC";
    public static String METHOD_NAME_getEnergyStorageInfo_SOC = "getEnergyStorageInfo_SOC";



    // EMS ACTION
    // In to ems
    public static String SOAP_ACTION_c800 = "c800";
    public static String METHOD_NAME_c800 = "c800";

    // Start charge
    public static String SOAP_ACTION_c801 = "c801";
    public static String METHOD_NAME_c801 = "c801";

    // Force stop charge
    public static String SOAP_ACTION_c830 = "c830";
    public static String METHOD_NAME_c830 = "c830";

    // Start release
    public static String SOAP_ACTION_c802 = "c802";
    public static String METHOD_NAME_c802 = "c802";

    // Force stop release
    public static String SOAP_ACTION_c803 = "c803";
    public static String METHOD_NAME_c803 = "c803";

    // change charge value
    public static String SOAP_ACTION_c840 = "c840";
    public static String METHOD_NAME_c840 = "c840";

    // change release value
    public static String SOAP_ACTION_c804 = "c804";
    public static String METHOD_NAME_c804 = "c804";

    // change return car soc
    public static String SOAP_ACTION_c805 = "c805";
    public static String METHOD_NAME_c805 = "c805";




    // function
    public static String doubleDecimalFormat (String origin) {
        if (origin.length() <=0) {
            return origin;
        }
        double value = Double.valueOf(origin);
        DecimalFormat df = new DecimalFormat("#.#");
        return df.format(value);
    }
}
