package com.bkeen.ems;

import android.content.DialogInterface;
import android.graphics.drawable.AnimationDrawable;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.text.BoringLayout;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.lidroid.xutils.ViewUtils;
import com.lidroid.xutils.view.annotation.ViewInject;

import org.ksoap2.serialization.SoapObject;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.util.HashMap;

/**
 * A placeholder fragment containing a simple view.
 */
public class EntertainmentActivityFragment extends Fragment implements Connect , View.OnTouchListener{
    private String TAG = "EntertainmentActivityFragment";

    @ViewInject(R.id.battery_power)
    private Button battery_power;

    @ViewInject(R.id.home_electronic)
    private Button home_electronic;

    @ViewInject(R.id.EVcar_power)
    private Button EVcar_power;

    @ViewInject(R.id.solar_energy_power)
    private Button solar_energy_power;

    @ViewInject(R.id.solar_animation)
    private ImageView solar_animation;

    @ViewInject(R.id.solar_energy_power_press)
    private ImageView solar_energy_power_press;

    @ViewInject(R.id.home_electronic_animation)
    private ImageView home_electronic_animation;

    @ViewInject(R.id.home_electronic_press)
    private ImageView home_electronic_press;

    @ViewInject(R.id.battery_power_animation)
    private ImageView battery_power_animation;

    @ViewInject(R.id.battery_power_press)
    private ImageView battery_power_press;

    @ViewInject(R.id.evcar_animation)
    private ImageView evcar_animation;

    @ViewInject(R.id.EVcar_power_press)
    private ImageView EVcar_power_press;

    //市電功率
    @ViewInject(R.id.textView_main_power)
    private TextView textView_main_power;

    //太陽能功率
    @ViewInject(R.id.textView_solar_energy_power)
    private TextView textView_solar_energy_power;

    //家電功率
    @ViewInject(R.id.textView_home_electronic)
    private TextView textView_home_electronic;

    //儲能系統功率
    @ViewInject(R.id.textView_battery_power)
    private TextView textView_battery_power;

    //電動車功率
    @ViewInject(R.id.textView_EVcar_power)
    private TextView textView_EVcar_power;

    //溫度
    @ViewInject(R.id.TextView_Temp)
    private TextView TextView_Temp;

    //濕度
    @ViewInject(R.id.TextView_Humidity)
    private TextView TextView_Humidity;

    //CO2
    @ViewInject(R.id.TextView_CO2)
    private TextView TextView_CO2;

    //CO2 中文
    @ViewInject(R.id.TextView_CO2_cht)
    private TextView TextView_CO2_cht;

    @ViewInject(R.id.main_view)
    RelativeLayout main_view;

    HttpAsyncTask asyncTask_ESS_Power;
    HttpAsyncTask asyncTask_EV_80A_Power;
    HttpAsyncTask asyncTask_Lighting_Power;
    HttpAsyncTask asyncTask_Solar_Power;
    HttpAsyncTask asyncTask_Sensor;
    HttpAsyncTask asyncTask_MainPower_Power;
    HttpAsyncTask asyncTask_ESS_charging;
    HttpAsyncTask asyncTask_DIO01;
    HttpAsyncTask asyncTask_DIO02;
    HttpAsyncTask asyncTask_DIO04;
    HttpAsyncTask asyncTask_DIO05;
    HttpAsyncTask asyncTask_DIO10;

    private Handler mHandler = new Handler();


    enum AnimationType{close,Charging,release}
    private AnimationType mCarAnimation = AnimationType.close;
    private AnimationType mESSAnimation = AnimationType.close;
    private AnimationType mSolarAnimation = AnimationType.close;
    private AnimationType mHomeAnimation = AnimationType.close;



    public EntertainmentActivityFragment() {

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        Log.d(TAG, "onCreateView");
        View view = inflater.inflate(R.layout.activity_entertainment, container, false);
        ViewUtils.inject(this,view);

        return view;
    }


    @Override
    public void onResume() {
        super.onResume();

        battery_power.setOnTouchListener(this);
        home_electronic.setOnTouchListener(this);
        EVcar_power.setOnTouchListener(this);
        solar_energy_power.setOnTouchListener(this);


        //設定Root_URL
        asyncTask_ESS_Power = new HttpAsyncTask(Config.ACTION_getESS_Power,EntertainmentActivityFragment.this,Config.HTTP_EMS_Root_URL,Config.HTTP_Meter,Config.ESS,Config.HTTP_Power);
        asyncTask_EV_80A_Power = new HttpAsyncTask(Config.ACTION_getEV_80A_Power,EntertainmentActivityFragment.this,Config.HTTP_EMS_Root_URL,Config.HTTP_Meter,Config.EV_80A,Config.HTTP_Power);
        asyncTask_Lighting_Power= new HttpAsyncTask(Config.ACTION_getLighting_Power,EntertainmentActivityFragment.this,Config.HTTP_EMS_Root_URL,Config.HTTP_Meter,Config.Lighting,Config.HTTP_Power);
        asyncTask_Solar_Power= new HttpAsyncTask(Config.ACTION_getSolar_Power,EntertainmentActivityFragment.this,Config.HTTP_EMS_Root_URL,Config.HTTP_Meter,Config.Solar,Config.HTTP_Power);
        asyncTask_Sensor= new HttpAsyncTask(Config.ACTION_getSensor,EntertainmentActivityFragment.this,Config.HTTP_EMS_Root_URL,Config.HTTP_Sensor);
        asyncTask_MainPower_Power= new HttpAsyncTask(Config.ACTION_getMainPower_Power,EntertainmentActivityFragment.this,Config.HTTP_EMS_Root_URL,Config.HTTP_Meter,Config.MainPower,Config.HTTP_Power);
        //asyncTask_EV_80A_charging = new HttpAsyncTask(Config.ACTION_getEV_80A_charging,EntertainmentActivityFragment.this,Config.HTTP_EMS_Root_URL,Config.HTTP_EnergyStorage,Config.EV_80A/*,Config.HTTP_Power*/);
        asyncTask_ESS_charging = new HttpAsyncTask(Config.ACTION_getESS_charging,EntertainmentActivityFragment.this,Config.HTTP_EMS_Root_URL,Config.HTTP_EnergyStorage,Config.ESS/*,Config.HTTP_Power*/);
        asyncTask_DIO01 = new HttpAsyncTask(Config.ACTION_getDIO01_info,EntertainmentActivityFragment.this,Config.HTTP_EMS_Root_URL,Config.HTTP_DigitalIO,"1");
        asyncTask_DIO02 = new HttpAsyncTask(Config.ACTION_getDIO02_info,EntertainmentActivityFragment.this,Config.HTTP_EMS_Root_URL,Config.HTTP_DigitalIO,"2");
        asyncTask_DIO04 = new HttpAsyncTask(Config.ACTION_getDIO04_info,EntertainmentActivityFragment.this,Config.HTTP_EMS_Root_URL,Config.HTTP_DigitalIO,"4");
        asyncTask_DIO05 = new HttpAsyncTask(Config.ACTION_getDIO05_info,EntertainmentActivityFragment.this,Config.HTTP_EMS_Root_URL,Config.HTTP_DigitalIO,"5");
        asyncTask_DIO10 = new HttpAsyncTask(Config.ACTION_getDIO10_info,EntertainmentActivityFragment.this,Config.HTTP_EMS_Root_URL,Config.HTTP_DigitalIO,"10");

        mHandler.post(getValue);
        mHandler.post(getDIOValue);
        mHandler.post(showAnimation);

    }


    @Override
    public void onDestroy() {
        Log.d(TAG, "onDestroy");
        super.onDestroy();
        mHandler.removeCallbacks(getDIOValue);
        mHandler.removeCallbacks(getValue);
    }

    Runnable getDIOValue = new Runnable() {
        @Override
        public void run() {

            asyncTask_DIO01.connect();
            asyncTask_DIO02.connect();
            asyncTask_DIO04.connect();
            asyncTask_DIO05.connect();
            asyncTask_DIO10.connect();
            mHandler.postDelayed(this, 2000);
        }
    };


    Runnable getValue = new Runnable() {
        @Override
        public void run() {

            //加入參數 HTTP_Meter = Meter , ESS = ESS , HTTP_Power = Power
            //最終Get路徑 :HTTP_EMS_Root_URL / HTTP_Meter / ESS / Power
            asyncTask_ESS_Power.connect();
            asyncTask_EV_80A_Power.connect();
            asyncTask_Lighting_Power.connect();
            asyncTask_Solar_Power.connect();
            asyncTask_Sensor.connect();
            asyncTask_MainPower_Power.connect();
            asyncTask_ESS_charging.connect();
            mHandler.postDelayed(this, 2000);
        }
    };

    int Frame = 0;
    Runnable showAnimation = new Runnable() {
        @Override
        public void run() {

            switch (mCarAnimation){
                case close:
                    evcar_animation.setImageResource(0);
                    break;
                case Charging:
                    switch (Frame){
                        case 0:
                            evcar_animation.setImageResource(R.drawable.charging_br01_powermanagement);
                            break;
                        case 1:
                            evcar_animation.setImageResource(R.drawable.charging_br02_powermanagement);
                            break;
                        case 2:
                            evcar_animation.setImageResource(R.drawable.charging_br03_powermanagement);
                            break;
                        case 3:
                            evcar_animation.setImageResource(R.drawable.charging_br04_powermanagement);
                            break;
                    }
                    break;
                case release:
                    switch (Frame){
                        case 0:
                            evcar_animation.setImageResource(R.drawable.release_br01_powermanagement);
                            break;
                        case 1:
                            evcar_animation.setImageResource(R.drawable.release_br02_powermanagement);
                            break;
                        case 2:
                            evcar_animation.setImageResource(R.drawable.release_br03_powermanagement);
                            break;
                        case 3:
                            evcar_animation.setImageResource(R.drawable.release_br04_powermanagement);
                            break;
                    }
                    break;
            }


            switch (mESSAnimation){
                case close:
                    battery_power_animation.setImageResource(0);
                    break;
                case Charging:
                    switch (Frame){
                        case 0:
                            battery_power_animation.setImageResource(R.drawable.charging_bl01_powermanagement);
                            break;
                        case 1:
                            battery_power_animation.setImageResource(R.drawable.charging_bl02_powermanagement);
                            break;
                        case 2:
                            battery_power_animation.setImageResource(R.drawable.charging_bl03_powermanagement);
                            break;
                        case 3:
                            battery_power_animation.setImageResource(R.drawable.charging_bl04_powermanagement);
                            break;
                    }
                    break;
                case release:
                    switch (Frame){
                        case 0:
                            battery_power_animation.setImageResource(R.drawable.release_bl01_powermanagement);
                            break;
                        case 1:
                            battery_power_animation.setImageResource(R.drawable.release_bl02_powermanagement);
                            break;
                        case 2:
                            battery_power_animation.setImageResource(R.drawable.release_bl03_powermanagement);
                            break;
                        case 3:
                            battery_power_animation.setImageResource(R.drawable.release_bl04_powermanagement);
                            break;
                    }
                    break;
            }

            switch (mSolarAnimation){
                case close:
                    solar_animation.setImageResource(0);
                    break;
                case Charging:
                    switch (Frame){
                        case 0:
                            solar_animation.setImageResource(R.drawable.release_tl01_powermanagement);
                            break;
                        case 1:
                            solar_animation.setImageResource(R.drawable.release_tl02_powermanagement);
                            break;
                        case 2:
                            solar_animation.setImageResource(R.drawable.release_tl03_powermanagement);
                            break;
                        case 3:
                            solar_animation.setImageResource(R.drawable.release_tl04_powermanagement);
                            break;
                    }
                    break;
            }

            switch (mHomeAnimation){
                case close:
                    home_electronic_animation.setImageResource(0);
                    break;
                case Charging:
                    switch (Frame){
                        case 0:
                            home_electronic_animation.setImageResource(R.drawable.charging_tr01_powermanagement);
                            break;
                        case 1:
                            home_electronic_animation.setImageResource(R.drawable.charging_tr02_powermanagement);
                            break;
                        case 2:
                            home_electronic_animation.setImageResource(R.drawable.charging_tr03_powermanagement);
                            break;
                        case 3:
                            home_electronic_animation.setImageResource(R.drawable.charging_tr04_powermanagement);
                            break;
                    }
                    break;
            }

            Frame++;
            if(Frame > 3){
                Frame = 0;
            }
            mHandler.postDelayed(this, 175);
        }
    };


    //Soap回傳
    @Override
    public void onResult(String SOAP_ACTION, SoapObject result) {
        Log.d(TAG, "onResult:" + SOAP_ACTION);
        if(result == null){
            return;
        }

    }

    //Http回傳
    @Override
    public void onHttpResult(String HTTP_ACTION, String result) {

        if(HTTP_ACTION.equals(Config.ACTION_getSensor)){    //取得三合一
            try {

                HashMap<String,Double> retList = new Gson().fromJson(result.replace("[","{").replace("]","}"),
                        new TypeToken<HashMap<String,Double>>() {
                        }.getType());
                Log.d(TAG, "CO2:" + retList.get("CO2"));
                Log.d(TAG, "Humi:" + retList.get("Humi"));
                Log.d(TAG, "Temp:" + retList.get("Temp"));

                TextView_Temp.setText("" + retList.get("Humi"));
                TextView_Humidity.setText(""+retList.get("Temp"));

                try {
                    //當PPM超過800，則為較差；800以下則為良好
                    double value = retList.get("CO2");
                    TextView_CO2.setText(Config.doubleDecimalFormat(""+retList.get("CO2")));

                    if(value > 800){
                        TextView_CO2_cht.setText("較差");
                    }else{
                        TextView_CO2_cht.setText("良好");
                    }
                }catch (Exception e){

                    TextView_CO2_cht.setText("");
                }

            } catch (Exception e) {
                e.printStackTrace();
            }

        }else if(HTTP_ACTION.equals(Config.ACTION_getESS_Power)){   //取得儲能系統功率
            Log.d(TAG, HTTP_ACTION + " : " + result);
            Config.ENERGY_STORAGE_VALUE = Config.doubleDecimalFormat(result);

            textView_battery_power.setText(Config.ENERGY_STORAGE_VALUE);


            double value = Double.valueOf(Config.doubleDecimalFormat(result));
            if(value < 0){
                mESSAnimation = AnimationType.release;
            } else if(value == 0){
                mESSAnimation = AnimationType.close;
            }else if(value > 0){
                mESSAnimation = AnimationType.Charging;
            }


        }else if(HTTP_ACTION.equals(Config.ACTION_getEV_80A_Power)){    //取得電動車功率
            Log.d(TAG, HTTP_ACTION + " : " + result);
            Config.ELECTRIC_CAR_VALUE = Config.doubleDecimalFormat(result);
            textView_EVcar_power.setText(Config.doubleDecimalFormat(result));

            double value = Double.valueOf(Config.doubleDecimalFormat(result));

            if(value < 0){
                mCarAnimation = AnimationType.release;
            } else if(value == 0){
                mCarAnimation = AnimationType.close;
            }else if(value > 0){
                mCarAnimation = AnimationType.Charging;
            }

        }else if(HTTP_ACTION.equals(Config.ACTION_getLighting_Power)){  //取得家電功率
            Log.d(TAG, HTTP_ACTION + " : " + result);

            //家電設備永遠都是橘色由下往上，無數值回傳時則無動態圖。
            try {

                double value = Double.valueOf(result);

                value = new BigDecimal(value)
                        .setScale(1, BigDecimal.ROUND_HALF_UP)
                        .doubleValue();
                textView_home_electronic.setText(""+value);
                if(value > 0)
                    mHomeAnimation = AnimationType.Charging;
                else
                    mHomeAnimation = AnimationType.close;

            }catch (Exception e){
                e.printStackTrace();
                mHomeAnimation = AnimationType.close;
            }

        }else if(HTTP_ACTION.equals(Config.ACTION_getSolar_Power)){  //取得太陽能功率
            Log.d(TAG, HTTP_ACTION + " : " + result);
            textView_solar_energy_power.setText("-" + result);


            //太陽能動態圖永遠都是綠色由上往下，無數值回傳時則無動態圖。
            try {

                double value = Double.valueOf(result);

                if(value > 0)
                    mSolarAnimation = AnimationType.Charging;
                else
                    mSolarAnimation = AnimationType.close;

            }catch (Exception e){
                e.printStackTrace();
                mSolarAnimation = AnimationType.close;
            }

        }else if(HTTP_ACTION.equals(Config.ACTION_getMainPower_Power)){ //取得市電功率
            Log.d(TAG, HTTP_ACTION + " : " + result);

            try{
                double value = Double.valueOf(result);
                value = new BigDecimal(value)
                        .setScale(1, BigDecimal.ROUND_HALF_UP)
                        .doubleValue();
                textView_main_power.setText(""+value);

            }catch (Exception e){
                textView_main_power.setText("0.0");
            }

        }else if(HTTP_ACTION.equals(Config.ACTION_getEV_80A_charging)) { //取得電動車充放電狀態
            Log.d(TAG, HTTP_ACTION + " : " + result);

            try{
                //true：充電中   false：放電中
                HashMap<String,String> retList = new Gson().fromJson(result.replace("[","{").replace("]","}"),
                        new TypeToken<HashMap<String,String>>() {
                        }.getType());
                String charge = retList.get("Charge");
                Config.ELECTRIC_CAR_STATE = Boolean.valueOf(charge);

            }catch (Exception e){
                e.printStackTrace();

            }

        }else if(HTTP_ACTION.equals(Config.ACTION_getESS_charging)) { //取得儲能系統充放電狀態
            Log.d(TAG, HTTP_ACTION + " : " + result);

            try{
                //true：充電中   false：放電中
                HashMap<String,String> retList = new Gson().fromJson(result.replace("[", "{").replace("]", "}"),
                        new TypeToken<HashMap<String, String>>() {
                        }.getType());
                String charge = retList.get("Charge");
                Config.ENERGY_STORAGE_SOC = retList.get("SOC");
                Config.ENERGY_STORAGE_STATE = Boolean.valueOf(charge);

            }catch (Exception e){
                e.printStackTrace();

            }
        }else if(HTTP_ACTION.equals(Config.ACTION_getDIO01_info)) { //取得展示燈狀態

            Log.d(TAG, HTTP_ACTION + " : " + result);
            try {
                Config.DIO01_state = Boolean.valueOf(result.replace("[","").replace("]", ""));
            }catch (Exception e){

            }

        }else if(HTTP_ACTION.equals(Config.ACTION_getDIO02_info)) { //取得走道燈狀態
            Log.d(TAG, HTTP_ACTION + " : " + result);
            try {
                Config.DIO02_state = Boolean.valueOf(result.replace("[","").replace("]",""));
            }catch (Exception e){

            }

        }else if(HTTP_ACTION.equals(Config.ACTION_getDIO04_info)) { //取得嵌燈狀態
            Log.d(TAG, HTTP_ACTION + " : " + result);
            try {
                Config.DIO04_state = Boolean.valueOf(result.replace("[","").replace("]",""));
            }catch (Exception e){

            }

        }else if(HTTP_ACTION.equals(Config.ACTION_getDIO05_info)) { //取得入口燈狀態
            Log.d(TAG, HTTP_ACTION + " : " + result.replace("[","").replace("]",""));
            try {
                Config.DIO05_state = Boolean.valueOf(result);
            }catch (Exception e){

            }

        }else if(HTTP_ACTION.equals(Config.ACTION_getDIO10_info)) { //取得冷氣狀態
            Log.d(TAG, HTTP_ACTION + " : " + result);
            try {
                Config.DIO10_state = Boolean.valueOf(result.replace("[","").replace("]",""));
            }catch (Exception e){

            }

        }
    }

    @Override
    public boolean onTouch(View view, MotionEvent motionEvent) {

        Log.d(TAG, "onClick");
        switch (view.getId()){

            case R.id.battery_power:  //儲能系統
                if (motionEvent.getAction() == MotionEvent.ACTION_DOWN) {  //按下的時候出現背景
                    battery_power_press.setVisibility(View.VISIBLE);

                }

                if(motionEvent.getAction() == MotionEvent.ACTION_UP){   //放開時候隱藏背景
                    mHandler.removeCallbacks(getDIOValue);
                    mHandler.removeCallbacks(getValue);
                    battery_power_press.setVisibility(View.INVISIBLE);
                    EnergyStorageDialog dialog = new EnergyStorageDialog(this.getContext());
                    dialog.setOnDismissListener(new DialogInterface.OnDismissListener() {

                        @Override
                        public void onDismiss(DialogInterface dialog) {
                            main_view.setVisibility(View.VISIBLE);
                            mHandler.post(getDIOValue);
                            mHandler.post(getValue);
                        }
                    });
                    main_view.setVisibility(View.INVISIBLE);
                    dialog.show();
                }

                break;

            case R.id.home_electronic:  //家電設備
                if (motionEvent.getAction() == MotionEvent.ACTION_DOWN) {  //按下的時候出現背景
                    home_electronic_press.setVisibility(View.VISIBLE);
                }

                if(motionEvent.getAction() == MotionEvent.ACTION_UP){   //放開時候隱藏背景

                    mHandler.removeCallbacks(getDIOValue);
                    mHandler.removeCallbacks(getValue);
                    home_electronic_press.setVisibility(View.INVISIBLE);
                    DigitalIODialog dialog = new DigitalIODialog(this.getActivity());
                    dialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
                        @Override
                        public void onDismiss(DialogInterface dialogInterface) {
                            main_view.setVisibility(View.VISIBLE);
                            mHandler.post(getDIOValue);
                            mHandler.post(getValue);
                        }
                    });
                    main_view.setVisibility(View.INVISIBLE);
                    dialog.show();
                }
                break;

            case R.id.EVcar_power:  //電動車

                if (motionEvent.getAction() == MotionEvent.ACTION_DOWN) {  //按下的時候出現背景
                    EVcar_power_press.setVisibility(View.VISIBLE);

                }

                if(motionEvent.getAction() == MotionEvent.ACTION_UP){   //放開時候隱藏背景

                    mHandler.removeCallbacks(getDIOValue);
                    mHandler.removeCallbacks(getValue);
                    EVcar_power_press.setVisibility(View.INVISIBLE);
                    ElectricCarDialog dialog = new ElectricCarDialog(this.getContext());
                    dialog.setOnDismissListener(new DialogInterface.OnDismissListener() {

                        @Override
                        public void onDismiss(DialogInterface dialog) {
                            main_view.setVisibility(View.VISIBLE);

                            mHandler.post(getDIOValue);
                            mHandler.post(getValue);
                        }
                    });
                    main_view.setVisibility(View.INVISIBLE);
                    dialog.show();
                }

                break;
            case R.id.solar_energy_power:   //太陽能

                if (motionEvent.getAction() == MotionEvent.ACTION_DOWN) {  //按下的時候出現背景
                    solar_energy_power_press.setVisibility(View.VISIBLE);

                }

                if(motionEvent.getAction() == MotionEvent.ACTION_UP){   //放開時候隱藏背景
                    solar_energy_power_press.setVisibility(View.INVISIBLE);

                }

                break;

        }

        return false;
    }
}