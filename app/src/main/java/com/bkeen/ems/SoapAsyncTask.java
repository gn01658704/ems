package com.bkeen.ems;

import android.os.AsyncTask;
import android.util.Log;

import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapPrimitive;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;

/**
 * Created by Pototype on 2015/11/1.
 */
public class SoapAsyncTask extends AsyncTask<String, Void, SoapObject> {
    private String TAG = "SoapAsyncTask";
    private Connect mConnect;
    private SoapObject request;
    private String Action = "";

    public SoapAsyncTask(String action,Connect connect,SoapObject request) {
        this.Action = action;
        this.mConnect = connect;
        this.request = request;
    }

    @Override
    protected SoapObject doInBackground(String... params) {
        Log.d(TAG,"SoapAsyncTask start:");
        SoapSerializationEnvelope envelope = new
                SoapSerializationEnvelope(SoapEnvelope.VER11);
        envelope.bodyOut = request;
        envelope.dotNet = true;
        envelope.setOutputSoapObject(request);
        HttpTransportSE androidHttpTransport = new HttpTransportSE(params[0]);
        androidHttpTransport.debug = true;

        try {
            if (params[1].isEmpty()) {
                androidHttpTransport.call(null, envelope);
            } else {
                androidHttpTransport.call(params[1], envelope);
            }
            if (envelope.getResponse() != null) {
                SoapObject resultsRequestSOAP = (SoapObject) envelope.bodyIn;
                Log.d(TAG,"Action:"+ params[1] + " Response:"+resultsRequestSOAP.toString());
                return resultsRequestSOAP;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }


    @Override
    protected void onPostExecute(SoapObject result) {
        mConnect.onResult(Action,result);
        super.onPostExecute(result);
    }
}
