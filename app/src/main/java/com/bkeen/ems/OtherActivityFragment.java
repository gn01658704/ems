package com.bkeen.ems;

import android.content.DialogInterface;
import android.graphics.drawable.AnimationDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.lidroid.xutils.ViewUtils;
import com.lidroid.xutils.view.annotation.ViewInject;

import org.ksoap2.serialization.SoapObject;

import java.math.BigDecimal;
import java.util.HashMap;

/**
 * A placeholder fragment containing a simple view.
 */
public class OtherActivityFragment extends Fragment {
    private String TAG = "OtherActivityFragment";

    public OtherActivityFragment() {

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        Log.d(TAG, "onCreateView");
        View view = inflater.inflate(R.layout.activity_other, container, false);
        ViewUtils.inject(this,view);

        return view;
    }


    @Override
    public void onResume() {
        super.onResume();

    }

}

